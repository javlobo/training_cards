import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import Header from './components/Header';
import CardList from './components/CardList/CardList';
import Card from './components/Card';
import Footer from './components/Footer';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
      data: [],
      isloading: true
    }
  }

  componentDidMount() {
    fetch('https://randomuser.me/api/?results=30')
      .then((response) => {
        return response.json();
      }).then((data) => {
        this.setState({
          data: data.results,
          isloading: false
        });
      }).catch(() => {
          // console.log('error', error);
      });
  }

  render() {
    // console.log(this.state.data);
    return (
      <div className="App">
        <Header />
        <CardList list={this.state.data} isLoading={this.state.isloading}/>
        <Footer />
      </div>
    );
  }
}

export default App;
