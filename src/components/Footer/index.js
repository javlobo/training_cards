import React, { Component } from 'react';

import PropTypes from "prop-types";

import './Footer.css'

const propTypes = { title: PropTypes.string };

const defaultProps = {
    "title": "KarmaPulse ©"
}


const Footer = (props) => {
    return (
        <footer>
            <p>{props.title}</p>
        </footer>
    );
}

Footer.propTypes = propTypes;
Footer.defaultProps = defaultProps;

export default Footer;