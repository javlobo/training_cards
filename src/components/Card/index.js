import React, { Component } from 'react';
import PropTypes from "prop-types";
import './Card.css';

const propTypes = {
    picture: PropTypes.shape({
        large: PropTypes.string
    }),
    name: PropTypes.shape({
        title: PropTypes.string,
        first: PropTypes.string,
        last: PropTypes.string
    }),
    location: PropTypes.shape({
        street: PropTypes.string,
        city: PropTypes.string
    }),
    email: PropTypes.string
};

const defaultProps = {
    picture:
        {
          large: "https://randomuser.me/api/portraits/men/89.jpg",
        },
    name:
        {
        title: "Mr",
        first: "Fernado",
        last: "Guevara"
        },
    location:
        {
        street: "Ayutla",
        city: "Ciudad de México"
        },
    email: "fernando@karmapulse.com",
};

const Card = (props) => {
    return (
        <div className="Card">
            <div className="Card__avatar">
                <img src={props.picture.large} />
            </div>
            <div className="Card__info">
                <h1>{props.name.title} {props.name.first} {props.name.last}</h1>
                <p>{props.location.street}, {props.location.city}</p>
                <div className="Card__info__options">
                    <p><a href="#">{props.email}</a></p>
                </div>
            </div>
        </div>
    );
}

Card.propTypes = propTypes;
Card.defaultProps = defaultProps;

export default Card;
