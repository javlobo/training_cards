import React, {Component} from 'react';
import PropTypes from "prop-types";
import Card from '../Card';

const propTypes = {
  list: PropTypes.array
};

const defaultProps = {
  list: []
};

const CardList = (props) => {
    const { isLoading, list } = props;
    return(
      <div className="containerCards">
        {
          isLoading 
            ? <div className="containerCards__loading"><h1>Loading...</h1></div>
            : props.list.map((user, index) => (
              <Card
              key={index}
              picture={user.picture}
              name={user.name}
              location={user.location}
              email={user.email}
            />
            ))
        }
      </div>
    )
}

CardList.propTypes = propTypes;
CardList.defaultProps = defaultProps;

export default CardList;
