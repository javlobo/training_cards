import React, { Component } from 'react';

import PropTypes from "prop-types";

import './Header.css'

const propTypes = { title: PropTypes.string };

const defaultProps = {
    "title": "KarmaCards!"
}


const Header = (props) => {
    return (
        <header>
            <h1>{props.title}</h1>
        </header>
    );
}

Header.propTypes = propTypes;
Header.defaultProps = defaultProps;

export default Header;